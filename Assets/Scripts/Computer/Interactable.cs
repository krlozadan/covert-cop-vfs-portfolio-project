﻿using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(SpriteRenderer))]
public class Interactable : MonoBehaviour {

	private SpriteRenderer sr;
	private BoxCollider2D col;
	internal bool interactable = true;
	
	// Use this for initialization
	void Awake () 
	{
		sr = GetComponent<SpriteRenderer>();
		sr.enabled = false;
		col = GetComponent<BoxCollider2D>();
		col.isTrigger = true;
	}
	
	private void OnTriggerStay2D(Collider2D other)
	{
		if(other.CompareTag("Player") && interactable)	
		{
			sr.enabled = true;
		}
		else 
		{
			sr.enabled = false;
		}
	}

	void OnTriggerExit2D(Collider2D other)
	{
		sr.enabled = false;
	}
}
