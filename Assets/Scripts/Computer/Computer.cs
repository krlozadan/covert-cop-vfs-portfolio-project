﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(LineRenderer))]
[RequireComponent(typeof(Animator))]
public class Computer : MonoBehaviour 
{
 
    #region Global Variables
    
    public static List<Computer> computerList = new List<Computer>();

    private IEnumerator currentState;
    private SpriteRenderer sr;
    private LineRenderer lr;
    private Animator animator;
    private float hackStatus;    
    private bool inHackingDistance = false;
    private bool hacking = false;
    private Interactable xButton;
    
    [Header("Hacking Configuration")]
    [SerializeField]
    private Sprite normalImg;
    [SerializeField]
    private Sprite inProgressImg;
    [SerializeField]
    private Sprite hackedImg;
    [SerializeField]
    [Range(1, 100)]
    private float hackingSpeed;
    [SerializeField]
    [Range(0, 100)]
    private float recoverSpeed;
 
 
    [Header("Computer Status Bar")]
    [SerializeField]
    private Color startColor;
    [SerializeField]
    private Color endColor;

    [Header("Checkpoint")]
    [SerializeField]
    [Tooltip("Drag a Game Object to be the new respawn point")]
    private GameObject spawnPosition;
 
    #endregion

    #region Unity Functions
    
    private void Awake()
    {
        sr = GetComponent<SpriteRenderer>();
        lr = GetComponent<LineRenderer>();
        animator = GetComponent<Animator>();
        xButton = GetComponentInChildren<Interactable>();
        if(spawnPosition == null)
        {
            Debug.LogWarning("There is no checkpoint attached to the computer");
        }
        SetState(StateNormal());
        computerList.Add(this);
    }
 
    private void Update()
    {    
        if(inHackingDistance)
        {
            if (Input.GetButton("Interact") || Input.GetKey(KeyCode.E))
            {
                if (!hacking && hackStatus < 100f)
                {	
                    hacking = true;
                    AudioManager.Instance.Play("Hacking");
                    SetState(StateHacking());
                }
            }
            else 
            {    
                if(hacking && hackStatus < 100f)
                {
					hacking = false;
                    AudioManager.Instance.Stop("Hacking");
                    SetState(StateRecovering());
                }
            }
        } 
        else
        {
            if(hacking && hackStatus < 100f)
            {
				hacking = false;
                AudioManager.Instance.Stop("Hacking");
                SetState(StateRecovering());
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.CompareTag("Player"))
        {
            inHackingDistance = true;
        }
    }
 
    private void OnTriggerExit2D(Collider2D other)
    {
        if(other.CompareTag("Player"))
        {
            inHackingDistance = false;
        }
    }

    public float GetHackStatus()
    {
        return hackStatus;
    }
 
    #endregion
 
    #region Status Bar
 
    private void UpdateStatusBar()
    {
        lr.SetPosition(0, new Vector3(-hackStatus / 50f, 5f, 0));
        lr.SetPosition(1, new Vector3(hackStatus / 50f, 5f, 0));
        lr.material.color = Color.Lerp(startColor, endColor, hackStatus / 100f);
    }    
 
    #endregion

    #region Hacking State Machine
 
    private void SetState(IEnumerator state)
    {
        if(currentState != null)
        {
            StopCoroutine(currentState);
        }
        currentState = state;
 
        if(currentState != null)
        {
            StartCoroutine(currentState);
        }
    }
 
    IEnumerator StateNormal()
    {
        lr.enabled = false;
        sr.sprite = normalImg;
        hackStatus = 0.0f;
        yield return null;
    }
 
    IEnumerator StateHacking()
    {
        if(hackStatus == 0)
        {
            lr.enabled = true;
            sr.sprite = inProgressImg;
        }
 
        while(true)
        {
            hackStatus += hackingSpeed * Time.deltaTime;
            UpdateStatusBar();
            if(hackStatus >= 100f)
            {
                SetState(StateHacked());    
            }
            yield return null;
        }
		
    }

    IEnumerator StateRecovering()
    {
        while(hackStatus > 0)
        {
            hackStatus -= recoverSpeed * Time.deltaTime;
            UpdateStatusBar();
            yield return null;
        }
        SetState(StateNormal());
    }
 
    IEnumerator StateHacked()
    {
        lr.enabled = false;
        sr.sprite = hackedImg;
        hackStatus = 100.0f;
        xButton.interactable = false;
        animator.SetBool("Hacked", true);
        SetState(null);
        if(GameManager.Instance != null)
        {
            AudioManager.Instance.Stop("Hacking");
            AudioManager.Instance.Play("ComputerGlitch");
            GameManager.Instance.AddComputerHacked();
            GameManager.Instance.CollectCheckpoint(spawnPosition.transform.position);
        }
        yield return null;
    }
 
    #endregion
}