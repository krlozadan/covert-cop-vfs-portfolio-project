﻿/*
* 
* Carlos Adan Cortes De la Fuente
* All rights reserved. Copyright (c) 
* Email: krlozadan@gmail.com
*
*/

using UnityEngine;
public class CameraPan : MonoBehaviour {

	#region Global Variables

		[SerializeField]
		private float limit = 5.0f;

		CameraFollow cameraFollow;

	#endregion

	#region Camera Offset 

		private void Awake()
		{
			cameraFollow = gameObject.GetComponent<CameraFollow>();
		}

		private void Update()
		{
			if(cameraFollow != null)
			{
				cameraFollow.SetOffset(Input.GetAxis("CameraX") * limit * 2, Input.GetAxis("CameraY") * limit);
			}
		}

	#endregion
}
