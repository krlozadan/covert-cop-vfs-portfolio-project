﻿/*
* Carlos Adan Cortes De la Fuente
* All rights reserved. Copyright (c) 
* Email: krlozadan@gmail.com
*/

/*
	Important:
	This class is used for position based checkpoints
 */

using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class Checkpoint : MonoBehaviour {

	private BoxCollider2D boxCol;
	private bool collected = false;

	// Use this for initialization
	void Awake () 
	{
		boxCol = GetComponent<BoxCollider2D>();
		boxCol.isTrigger = true;
	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		if(other.CompareTag("Player") && !collected)
		{
			if(GameManager.Instance != null)
			{
				collected = true;
				GameManager.Instance.CollectCheckpoint(gameObject.transform.position);
			}
		}
	}

}
