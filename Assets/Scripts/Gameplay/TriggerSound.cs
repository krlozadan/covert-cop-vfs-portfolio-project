﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(AudioSource))]
public class TriggerSound : MonoBehaviour 
{
	[SerializeField]
	private bool PlayMultipleTimes = false;
	private bool played = false;

	private AudioSource audioSource;
	private BoxCollider2D boxCol;

	private void Awake()
	{
		boxCol = GetComponent<BoxCollider2D>();
		boxCol.isTrigger = true;
		audioSource = GetComponent<AudioSource>();
	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		if(other.CompareTag("Player"))
		{
			if(audioSource.clip != null)
			{
				if(PlayMultipleTimes)
				{
					audioSource.Play();
				}
				else 
				{
					if(!played)
					{
						played = true;
						audioSource.Play();
					}
				}
			}
			else 
			{
				Debug.LogWarning("There is no audio clip attached!!");
			}
		}
	}
}
