﻿/*
* 
* Carlos Adan Cortes De la Fuente
* All rights reserved. Copyright (c) 
* Email: krlozadan@gmail.com
*
*/

using UnityEngine;

public class Platform : MonoBehaviour 
{
	#region Properties

	[Header("Movement")]
	[SerializeField]
	private bool movingPlatform = true;
	[SerializeField]
	private float speed = 1f;
	[SerializeField]
	private float xDistance = 5f;
	[SerializeField]
	private float yDistance = 5f;

	private TranslatingObject platformTO;	

	#endregion

	#region Unity Functions

	void Awake () 
	{
		platformTO = new TranslatingObject(speed, xDistance, yDistance);
	}
	
	void Update () 
	{
		if(movingPlatform)
		{
			transform.position = platformTO.Move(transform.position);
		}
	}

	private void OnCollisionEnter2D(Collision2D other)
	{
		if(other.transform.CompareTag("Player"))
		{
			if(other.contacts[0].normal == Vector2.down)
			{
				other.transform.SetParent(transform);	
			}
		}	
	}

	private void OnCollisionExit2D(Collision2D other)
	{
		if(other.transform.CompareTag("Player"))
		{
			other.transform.SetParent(null);
		}	
	}

	#endregion

	#region Class Methods

	

	#endregion
}