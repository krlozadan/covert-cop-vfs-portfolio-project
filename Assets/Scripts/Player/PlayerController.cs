﻿/*
* 
* Carlos Adan Cortes De la Fuente
* All rights reserved. Copyright (c) 
* Email: krlozadan@gmail.com
*
*/

using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(PolygonCollider2D))]
[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(Animator))]
public class PlayerController : MonoBehaviour 
{

	#region Global Variables

	private Rigidbody2D rb2d;
	private Animator animator;
	private PolygonCollider2D polyCollider;
	private Transform playerCenter;

	[Header("Player Movement")]
	[SerializeField]
	private float walkSpeed = 4f;
	[SerializeField]
	private float runSpeed = 3f;
	[SerializeField]
	private float crouchSpeed = 0.4f;
	
	private float xInput;
	private bool facingRight = true;
	private float movementMultiplier;
	private bool isCrouching = false;
	private Grounded grounded;

	[Space]
	[Header("Player Jump")]
	[SerializeField]
	private float jumpForce = 12f;
	[SerializeField]
	private int maxJumps = 2;
	[SerializeField]
	private float midairSpeed = 2f;

	private bool jumpPressed;
	private int jumps;
	private bool inMidair = false;
	private bool isGrounded = false;

	[Header("Wall Jump & Slide")]
	[SerializeField]
	private float slideSpeed = 2.0f;
	[SerializeField]
	private Vector2 wallJumpDirection;
	[SerializeField]
	private float stickWallTime = 0.4f;
	private bool wallSliding = false;
	private bool wallJump = false;
	private float lastWallJumpTimeStamp;

	private enum ColliderShape {
		Standing,
		Crouching,
		Jumping,
		DoubleJumping
	};

	#endregion

	public float xoff = 1;
	public float yoff = 1;

	#region Unity Funcitons

	private void Awake()
	{
		rb2d = GetComponent<Rigidbody2D>();
		animator = GetComponent<Animator>();
		polyCollider = GetComponent<PolygonCollider2D>();
		playerCenter = transform.Find("PlayerCenter");
		grounded = new Grounded();
	}

	private void Update()
	{			
		SetGrounded(); 
		CheckGrounded();
		GetMoveInput();
		SetSliding();
		FlipCharacter();
		Crouch();
		Jump();
		MoveCharacter();
	}

	private void FixedUpdate()
	{
		ChangeRaycast();
		ApplyMovementPhysics();
	}

	#endregion


	#region Class Functions

	private void SetGrounded()
	{	
		isGrounded = grounded.IsGrounded(transform.position, false);
		animator.SetBool("Grounded", isGrounded);
		if(isGrounded && !isCrouching)
		{
			wallSliding = false;
			animator.SetBool("Wallsliding", wallSliding);
			ChangeColider(ColliderShape.Standing);
		}
	}

	private void SetSliding()
	{
		if(!isGrounded && rb2d.velocity.y < 0 && !wallJump)
		{
			wallSliding = Physics2D.Raycast(playerCenter.position, Vector3.right * xInput, 0.75f, LayerMask.GetMask("Default"));
			animator.SetBool("Wallsliding", wallSliding);
			Debug.DrawRay(playerCenter.position, Vector3.right * xInput * 0.75f, Color.red);
		}
	}

	private void CheckGrounded() 
	{
		if (isGrounded && rb2d.velocity.y == 0 && !jumpPressed) 
		{ 
			jumps = 0; 
			inMidair = false; 
			animator.SetInteger("Jumps", jumps); 
		}
	}

	private void GetMoveInput()
	{
		movementMultiplier = 1;
		if(Time.time >= lastWallJumpTimeStamp + stickWallTime)
		{
			xInput = Input.GetAxis("Horizontal"); // value from -1 to 1	
		}
	}

	private void FlipCharacter() 
	{
		if (xInput > 0 && !facingRight || xInput < 0 && facingRight) 
		{
			facingRight = !facingRight;
			Vector3 newScale = transform.localScale;
			newScale.x *= -1;
			transform.localScale = newScale;
		}
	}

	private void ApplyMovementPhysics()
	{
		Vector2 newVelocity = new Vector2();
		if(wallSliding)
		{
			newVelocity.x = 0;
			if(rb2d.velocity.y < -slideSpeed)
			{
				rb2d.velocity = new Vector2(rb2d.velocity.y, -slideSpeed);
			}
		}
		else
		{
			float speed = xInput * walkSpeed * movementMultiplier;
			animator.SetFloat("Speed", Mathf.Abs(speed));
			newVelocity.x = speed;
		}

		if(jumpPressed)
		{
			newVelocity.y = jumpForce;
		}
		else if (wallJump)
		{
			wallSliding = false;
			animator.SetBool("Wallsliding", wallSliding);
			newVelocity.x = -xInput * wallJumpDirection.x;
			newVelocity.y = wallJumpDirection.y;
			xInput *= -1;
			lastWallJumpTimeStamp= Time.time;
		}
		else
		{
			newVelocity.y = rb2d.velocity.y;
			if(jumps == 1)
			{
				ChangeColider(ColliderShape.Jumping);
			} 
			else if(jumps > 1)
			{
				ChangeColider(ColliderShape.DoubleJumping);
			}
		}
		rb2d.velocity = newVelocity;
		jumpPressed = false;
		wallJump = false;
	}

	private void Crouch()
	{
		if ((Input.GetKeyDown(KeyCode.LeftControl) || Input.GetButtonDown("Crouch")) && isGrounded)
		{				
			ToggleCrouch();
		}
	}

	private void Jump()
	{
		if (Input.GetKeyDown(KeyCode.Space) || Input.GetButtonDown("Jump"))
		{
			if(wallSliding)
			{
				wallJump = true;
				jumps = maxJumps;
			}
			else
			{
				// First jump
				if (isGrounded)
				{
					if(isCrouching)
					{
						ToggleCrouch();
					}
					jumpPressed = true;
					inMidair = true;
					jumps++;
					AudioManager.Instance.Play("JumpVoice");
					animator.SetInteger("Jumps", jumps);
				} // Double Jump
				else if (inMidair && jumps <= maxJumps - 1)
				{
					jumpPressed = true;
					jumps++;
					AudioManager.Instance.Play("DoubleJump");
					animator.SetInteger("Jumps", jumps);
				}
			}
		}
	}

	private void MoveCharacter()
	{
		if (Input.GetKey(KeyCode.LeftShift) || Mathf.Abs(Input.GetAxis("Triggers")) > 0) 
		{
			if(isGrounded) 
			{
				movementMultiplier = runSpeed;
				if(isCrouching)
				{
					ToggleCrouch();
				}
			}
			else if (!isGrounded && inMidair) 
			{
				movementMultiplier = midairSpeed;
			}
		}

		if (isCrouching)
		{
			movementMultiplier = crouchSpeed;
		}
	}

	private void ToggleCrouch()
	{
		isCrouching = !isCrouching;
		if(!isCrouching)
		{
			ChangeColider(ColliderShape.Standing);
		}
		else
		{
			ChangeColider(ColliderShape.Crouching);
		}
		animator.SetBool("Crouching", isCrouching);
	}

	private void ChangeColider(ColliderShape shape)
	{
		Vector2[] points = polyCollider.points; 
		switch(shape)
		{
			case ColliderShape.Standing:
				points[0] = new Vector2(-0.55f, 0);
				points[1] = new Vector2(1.05f, 0);
				points[2] = new Vector2(1.05f, 9.0f);
				points[3] = new Vector2(-0.55f, 9.0f);
				break;
			case ColliderShape.Crouching:
				points[0] = new Vector2(-1.63f, 0.0f);
				points[1] = new Vector2(1.63f, 0.0f);
				points[2] = new Vector2(1.63f, 6.61f);
				points[3] = new Vector2(0.09f, 6.61f);
				break;
			case ColliderShape.Jumping:
				points[0] = new Vector2(-1.37f, 0.0f);
				points[1] = new Vector2(1.37f, 0.0f);
				points[2] = new Vector2(1.92f, 7.9f);
				points[3] = new Vector2(0.02f, 7.9f);
				break;
			case ColliderShape.DoubleJumping:
				points[0] = new Vector2(-1.37f, 0.0f);
				points[1] = new Vector2(1.37f, 0.0f);
				points[2] = new Vector2(1.37f, 5.0f);
				points[3] = new Vector2(-1.37f, 5.0f);
				break;
		}
		polyCollider.points = points;
	}

	private void ChangeRaycast()
	{
		if(isGrounded)
		{
			if (isCrouching)
			{
				grounded.SetOffset(0.47f, 0.002f);
			}
			else
			{	
				// Standing
				grounded.SetOffset(0.303f, 0.002f);
			}
		}
		else 
		{
			if(inMidair) // Jumping
			{
				if(jumps == 1)
				{
					grounded.SetOffset(0.39f, 0.002f);
				}
				else 
				{
					grounded.SetOffset(0.39f, 0.002f);
				}
			}
		}
	}

	#endregion 

}
