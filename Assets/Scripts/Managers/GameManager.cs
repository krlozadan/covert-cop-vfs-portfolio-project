﻿using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine;
using System.Collections.Generic;

public class GameManager : SingletonBehaviour<GameManager> 
{
	
	#region Global Variables
		
		private bool gamePaused = false;
		private int computersHacked = 0;
		private Vector3 checkpointPosition;
		private PlayerController player;
		private GameScreen gameScreen;

	#endregion


	#region Singleton Object

		protected override void SingletonAwake()
		{
			Physics2D.queriesHitTriggers = false; // This prevents rayscasts to hit colliders marked as triggers
			player = FindObjectOfType<PlayerController>();
			checkpointPosition = player.transform.position;
			gameScreen = FindObjectOfType<GameScreen>();
		}

	#endregion 

	#region Game Pause Logic

		public void PauseGame()
		{
			Time.timeScale = 0f;
			gamePaused = false;
		}
		
		public void ResumeGame()
		{
			Time.timeScale = 1f;
			gamePaused = true;
		}

		public bool IsGamePaused()
		{
			return gamePaused;
		}

	#endregion

	#region Game Over Logic

		public void GameOver()
		{
			Computer.computerList.Clear();
			SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
		}

	#endregion

	#region Player Killed

		public void KillPlayer()
		{
			player.transform.position = checkpointPosition;
		}

	#endregion

	#region Computers Hacked Logic
	
		public void AddComputerHacked()
		{
			computersHacked++;
			if(computersHacked == 1)
			{
				AudioManager.Instance.ChangeGameplayClip("InGameMusic", 1);
			}
			else if(computersHacked == 2)
			{
				AudioManager.Instance.ChangeGameplayClip("InGameMusic", 2);
			}
		}

		public void ResetComputers()
		{
			computersHacked = 0;
			Computer.computerList = new List<Computer>();
			gameScreen.ResetData();
			AudioManager.Instance.ChangeGameplayClip("InGameMusic", 0);
		}

		public void CollectCheckpoint(Vector3 position)
		{
			checkpointPosition = position;
		}

    #endregion
}
