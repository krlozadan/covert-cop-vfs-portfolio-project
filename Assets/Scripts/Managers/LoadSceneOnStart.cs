﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadSceneOnStart : MonoBehaviour 
{
	[SerializeField]
	private string sceneToLoad;

	private void Start()
	{
		sceneToLoad = sceneToLoad.Trim();
		if(sceneToLoad.Length > 0)
		{
			SceneManager.LoadScene(sceneToLoad);
		}
	}
}