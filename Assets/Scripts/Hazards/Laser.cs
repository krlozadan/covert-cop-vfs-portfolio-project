﻿/*
* 
* Carlos Adan Cortes De la Fuente
* All rights reserved. Copyright (c) 
* Email: krlozadan@gmail.com
*
*/

using System.Collections;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class Laser : MonoBehaviour 
{
	#region Properties

	private LineRenderer line;
	private Transform start;
	private Transform end;
	private AudioSource audioSrc;

	private RaycastHit2D hit;

	[Header("Laser Randomnes")]
	[SerializeField]
	private float laserStrength = 0.5f;
	[SerializeField, Range(3, 20)]
	private int laserBreaks = 15;

	[Header("Laser Switch")]
	[SerializeField]
	private bool switchingLaser = true;
	[SerializeField]
	private float timeOn = 3f;
	[SerializeField]
	private float timeOff = 3f;

	private bool isOn = false;

	[Header("Movement")]
	[SerializeField]
	private bool movingLaser = true;
	[Space]
	[SerializeField]
	private float startSpeed = 2f;
	[SerializeField]
	private float startXDis = 5f;
	[SerializeField]
	private float startYDis = 5f;
	
	[Space]
	[SerializeField]
	private float endSpeed = 2f;
	[SerializeField]
	private float endXDis = 5f;
	[SerializeField]
	private float endYDis = 5f;

	private TranslatingObject startTO;
	private TranslatingObject endTO;

	#endregion

	#region Unity Functions

	void Awake () 
	{
		start = transform.Find("Start");
		end = transform.Find("End");
		audioSrc = GetComponent<AudioSource>();

		if(movingLaser)
		{
			startTO = new TranslatingObject(Mathf.Abs(startSpeed), Mathf.Abs(startXDis), Mathf.Abs(startYDis));
			endTO = new TranslatingObject(Mathf.Abs(endSpeed), Mathf.Abs(endXDis), Mathf.Abs(endYDis));
		}

		line = GetComponent<LineRenderer>();
		if(switchingLaser)
		{
			StartCoroutine(SwitchLaser());
		}
	}
	
	void Update () 
	{
		if(movingLaser)
		{
			start.transform.localPosition = startTO.Move(start.transform.localPosition);
			end.transform.localPosition = endTO.Move(end.transform.localPosition);
		}
		DrawLaser();
	}

	#endregion

	#region Class Methods

	private void DrawLaser()
	{	
		if(isOn)
		{
			line.positionCount = laserBreaks;
			line.SetPosition(0, start.localPosition);
			line.SetPosition(line.positionCount - 1, end.localPosition);
			for (int i = 1; i < line.positionCount - 1; ++i)
			{
				Vector3 newPos = ((float)(line.positionCount - 1 - i) / (float)(line.positionCount - 1)) * start.localPosition + ((float)i / (float)(line.positionCount - 1)) * end.localPosition;
				newPos.x += Random.Range(-laserStrength, laserStrength);
				newPos.y += Random.Range(-laserStrength, laserStrength);
				line.SetPosition(i, newPos);
			}
			CheckLaserHit();
		}
		else
		{
			line.positionCount = 2;
			line.SetPosition(0, Vector2.zero);
			line.SetPosition(1, Vector2.zero);
		}
	}

	private void CheckLaserHit() 
	{
		Vector3 ab = -start.position + end.position;
		hit = Physics2D.Raycast(start.position, ab, ab.magnitude, LayerMask.GetMask("Player"));
		if(hit) 
		{
			AudioManager.Instance.Play("PlayerDeath");
			GameManager.Instance.KillPlayer();
		}
	}

	IEnumerator SwitchLaser()
    {
        while(true)
		{
			float time = isOn ? timeOff : timeOn;
			isOn = !isOn;
			if(isOn)
			{
				audioSrc.Play();	
			}
			else
			{
				audioSrc.Stop();
			}
			yield return new WaitForSeconds(time);
		}
    }

	#endregion
}