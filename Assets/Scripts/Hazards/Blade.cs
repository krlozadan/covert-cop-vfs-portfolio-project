﻿using UnityEngine;

[RequireComponent(typeof(CircleCollider2D))]
public class Blade : MonoBehaviour
{
	#region Variables

	[Header("Rotation")]
	[SerializeField]
	private float rotationSpeed = 50f;
	[SerializeField]
	private bool clockwise = true;

	private GameObject rotatingBlade;
	private RotatingObject bladeRO;


	[Space]
	[Header("Movement")]
	[SerializeField]
	private bool movingBlade = true;
	[SerializeField]
	private float moveSpeed = 1f;
	[SerializeField]
	private float xDistance = 5f;
	[SerializeField]
	private float yDistance = 5f;

	private TranslatingObject bladeTO;

	#endregion 
	
	#region Unity Functions
	
	void Awake()
	{
		rotatingBlade = transform.Find("RotatingBlades").gameObject;
		bladeRO = new RotatingObject(rotationSpeed, clockwise);
		bladeTO = new TranslatingObject(moveSpeed, xDistance, yDistance);
	}

	void Update() 
	{
		Rotate();
		if(movingBlade)
		{	
			transform.position = bladeTO.Move(transform.position);
		}
	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.CompareTag("Player"))
		{
			AudioManager.Instance.Play("PlayerDeath");
			GameManager.Instance.KillPlayer();
		}
	}

	#endregion

	#region Class Methods

	private void Rotate()
	{
		rotatingBlade.transform.Rotate(bladeRO.Rotate());
	}	

	#endregion


}



