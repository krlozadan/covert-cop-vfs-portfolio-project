﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FieldOfView : MonoBehaviour 
{

	#region Global Variables

	private IEnumerator currentState;
	private LayerMask playerMask;
	private LayerMask obstaclesMask;

	[SerializeField]
	private bool debugMode = true;
	[SerializeField]
	private float viewRadius;
	[SerializeField]
	[Range(0, 360)]
	private float viewAngle;
	[SerializeField]
	private float meshResolution = 0.5f;

	// We used a mesh to join all the points in space that where
	private struct MeshRaycastInfo 
	{
		public bool hit;
		public Vector3 point;
		public float distance;
		public float angle;

		public MeshRaycastInfo(bool bHit, Vector3 vPoint, float fDist, float fAngle)
		{
			hit = bHit;
			point = vPoint;
			distance = fDist;
			angle = fAngle;
		}
	}
	private MeshFilter meshFilter;
	private Mesh mesh;
	private List<Vector3> pointList;

	#endregion

	#region Unity Functions

	void Awake () 
	{
		playerMask = LayerMask.GetMask("Player");
		obstaclesMask = LayerMask.GetMask("Default");

		// Init Mesh
		pointList = new List<Vector3>();
		mesh = new Mesh();
		mesh.name = "Field Mesh";
		meshFilter = GetComponent<MeshFilter>();
		meshFilter.mesh = mesh;

		SetState(StateFindPlayer(0.1f));
	}

	void LateUpdate() 
	{
		DrawFieldOfView();
	}

	void OnDrawGizmos()
	{
		if(debugMode)
		{
			Gizmos.color = Color.green;
			Gizmos.DrawWireSphere(transform.position, viewRadius);
			Gizmos.color = Color.blue;
			Gizmos.DrawRay(transform.position, DirectionFromAngle(transform.eulerAngles.y));
			Gizmos.color = Color.cyan;
			Gizmos.DrawRay(transform.position, DirectionFromAngle(-viewAngle / 2));
			Gizmos.DrawRay(transform.position, DirectionFromAngle(viewAngle / 2));
		}
	}

	#endregion

	#region Class Methods

	private Vector3 DirectionFromAngle (float angle)
	{
		angle -= transform.eulerAngles.z; 	// Adapts the rotation angle to the z rotation of the game object
		Vector3 direction = new Vector3(Mathf.Sin(angle * Mathf.Deg2Rad), Mathf.Cos(angle * Mathf.Deg2Rad), 0);
		return direction * viewRadius;		// Adds the magnitude of the vector depending on the radius
	}

	private void FindPlayer()
	{
		Collider2D playerCollider = Physics2D.OverlapCircle(transform.position, Mathf.Abs(viewRadius), playerMask);
		// Player is inside the radius
		if(playerCollider != null)
		{
			// Get The player 
			Vector3 colliderPos = playerCollider.bounds.center;
			Vector3 extents = playerCollider.bounds.extents;
			Vector3[] bounds = {
				new Vector3(colliderPos.x + extents.x, colliderPos.y + extents.y, 0f),
				new Vector3(colliderPos.x + extents.x, colliderPos.y - extents.y, 0f),
				new Vector3(colliderPos.x - extents.x, colliderPos.y + extents.y, 0f),
				new Vector3(colliderPos.x - extents.x, colliderPos.y - extents.y, 0f)
			};

			foreach (Vector3 colPos in bounds)
			{
				Vector3 dirToPlayer = (colPos - transform.position).normalized;
				if(Vector3.Angle(DirectionFromAngle(0), dirToPlayer) < viewAngle / 2)
				{
					// Checks if there are any obstacles
					float distanceToPlayer = Vector3.Distance(transform.position, colPos);
					if(!Physics2D.Raycast(transform.position, dirToPlayer, distanceToPlayer, obstaclesMask) && distanceToPlayer <= viewRadius)
					{
						if(debugMode)
						{
							Debug.DrawLine(transform.position, colliderPos, Color.magenta);
						}
						AudioManager.Instance.Play("PlayerCaught");
						GameManager.Instance.KillPlayer();
					}
				}
			}
		}
	}

	private void DrawFieldOfView()
	{
		int rayCount = Mathf.RoundToInt(viewAngle * meshResolution);
		float angleSize = viewAngle / rayCount;

		pointList.Clear();
		for(int i = 0; i < rayCount; i++)
		{
			float angle = transform.eulerAngles.y - viewAngle / 2 + angleSize * i;
			MeshRaycastInfo newMRI = CreateMeshRay(angle);
			pointList.Add(newMRI.point);
			// Debug.DrawLine(transform.position, transform.position + DirectionFromAngle(angle), Color.red);
		}

		CreateFieldOfViewMesh();
	}

	private MeshRaycastInfo CreateMeshRay(float angle)
	{		
		Vector3 direction = DirectionFromAngle(angle);
		RaycastHit2D hit = Physics2D.Raycast(transform.position, direction, viewRadius, obstaclesMask);
		if(hit)
		{
			return new MeshRaycastInfo(true, hit.point, hit.distance, angle);
		}
		else
		{
			return new MeshRaycastInfo(false, transform.position + direction, viewRadius, angle);
		}
	}

	private void CreateFieldOfViewMesh()
	{
		int vertexCount = pointList.Count + 1; // We add the origin vertex
		Vector3[] vertices = new Vector3[vertexCount];
		int[] triangles = new int[(vertexCount - 2) * 3];

		vertices[0] = Vector3.zero;
		for (int i = 0; i < vertexCount - 1; i++)
		{
			vertices[i + 1] = transform.InverseTransformPoint(pointList[i]);
			if(i < vertexCount - 2)
			{
				triangles[i * 3] = 0;
				triangles[i * 3 + 1] = i + 1;
				triangles[i * 3 + 2] = i + 2;
			}
		}
		
		mesh.Clear();
		mesh.vertices = vertices;
		mesh.triangles = triangles;
		mesh.RecalculateNormals();
	}

	#endregion

	#region State Machine

	private void SetState(IEnumerator state)
	{
		if(currentState != null)
		{
			StopCoroutine(currentState);
		}
		currentState = state;
		StartCoroutine(currentState);
	}

	IEnumerator StateFindPlayer(float delay) // The delay is for performance 
	{
		while(true)
		{
			yield return new WaitForSeconds(delay);
			FindPlayer();
		}
	}

	#endregion
}
