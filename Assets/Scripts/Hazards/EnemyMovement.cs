﻿using System.Collections;
using UnityEngine;

public class EnemyMovement : MonoBehaviour 
{

	#region Global Variables

	[Header("Enemy Type")]
	[SerializeField]
	private bool movingEnemy = true;
	[SerializeField]
	[Tooltip("This value affects both types of enemy (Moving or Stationary)")]
	private float waitingTime = 2f;

	[Header("Enemy Movement")]
	[SerializeField]
	private bool debugMode = true;
	[SerializeField]
	private float walkSpeed = 2f;
	[SerializeField]
	private float leftDistance = 2f;
	[SerializeField]
	private float rightDistance = 2f;

	private Vector3 initialPos;
	private Vector3 leftPos;
	private Vector3 rightPos;

	private bool isWaiting = false;
	private IEnumerator currentState = null;	

	// Grounded properties
	private float rayCastOffsetX = 0.25f;
	private float rayCastOffsetY = 1.26f;
	private Grounded grounded;

	private float direction = 1f;
	private bool isGrounded = false;

	private Animator animator;
	private Rigidbody2D rb2d;
	

	#endregion


	#region Unity Functions
	
	void Awake () 
	{
		animator = GetComponent<Animator>();
		rb2d = GetComponent<Rigidbody2D>();
		grounded = new Grounded();
		grounded.SetOffset(rayCastOffsetX, rayCastOffsetY);

		initialPos = transform.position;
		leftPos = initialPos;
		leftPos.x -= Mathf.Abs(leftDistance);
		rightPos = initialPos;
		rightPos.x += Mathf.Abs(rightDistance);
		if(!movingEnemy)
		{
			SetState(StateLookBothWays());
		}
	}

	void Update () 
	{
		isGrounded = grounded.isSideGrounded(transform.position, direction);
		if (!isGrounded && !isWaiting)
		{
			SetState(StateWait());
		}

		if(movingEnemy && !isWaiting)
		{
			CheckDistances();
		}
	}

	private void FixedUpdate()
	{
		if(movingEnemy && !isWaiting)
		{
			SetState(StateWalk());
		}
	}

	private void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.CompareTag("Player"))
		{
			AudioManager.Instance.Play("PlayerCaught");
			GameManager.Instance.KillPlayer();
		}
	}

	#endregion

	#region Class Methods

	private bool IsGrounded(float xOffset, float yOffset) 
	{
		float raycastDistance = 0.1f;
		Vector2 origin = transform.position;
		origin.x += xOffset;
		origin.y -= yOffset;

		return Physics2D.Raycast(origin, Vector2.down, raycastDistance);
	}

	private void CheckDistances()
	{
		if(debugMode)
		{
			Debug.DrawLine(transform.position, leftPos, Color.red);
			Debug.DrawLine(transform.position, rightPos, Color.blue);
		}

		// Going right
		if(direction == 1f)
		{	
			if(HasReachedDestination(rightPos))
			{
				SetState(StateWait());
			}
		}

		// Going Left
		else if (direction == -1)
		{
			if(HasReachedDestination(leftPos))
			{
				SetState(StateWait());
			}
		}

	}

	private bool HasReachedDestination(Vector3 destination)
	{
		return Vector3.Magnitude(destination - transform.position) <= 0.5f;
	}

	private void TurnAround() 
	{
		direction *= -1;
		Vector3 newScale = transform.localScale;
		newScale.x *= -1;
		transform.localScale = newScale;
	}

	#endregion

	#region State Machine

	private void SetState(IEnumerator state)
	{
		if(currentState != null)
		{
			StopCoroutine(currentState);
		}
		currentState = state;
		StartCoroutine(currentState);
	}

	IEnumerator StateWalk()
	{
		rb2d.velocity = new Vector2(direction * walkSpeed, 0);
		animator.SetBool("Moving", movingEnemy);
		yield return null;
	}

	IEnumerator StateWait()
    {
		isWaiting = true;
		animator.SetBool("Moving", !isWaiting);
		yield return new WaitForSeconds(waitingTime);
		TurnAround();
		animator.SetBool("Moving", isWaiting);
		isWaiting = false;
		SetState(StateWalk());
    }

	IEnumerator StateLookBothWays()
    {
		while(true)
		{
			TurnAround();
			yield return new WaitForSeconds(waitingTime);
		}
    }

	#endregion
}
