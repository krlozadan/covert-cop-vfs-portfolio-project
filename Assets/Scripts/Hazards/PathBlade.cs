﻿using System.Collections.Generic;
using UnityEngine;

public class PathBlade : MonoBehaviour {

	#region Global Variables

	[Header("Rotation")]
	[SerializeField]
	private float rotationSpeed = 50f;
	[SerializeField]
	private bool clockwise = true;

	private GameObject rotatingBlade;
	private RotatingObject bladeRO;

	[Header("Translation")]
	[SerializeField]
	private bool debugMode = true;
	[SerializeField]
	private List<Vector3> navPoints;
	[SerializeField]
	private float movementSpeed = 5f;

	private int nextPoint = 1;

	#endregion

	#region Unity Functions

	void Awake () 
	{
		rotatingBlade = transform.Find("RotatingBlades").gameObject;
		bladeRO = new RotatingObject(rotationSpeed, clockwise);

		if(navPoints.Count > 1)
		{
			transform.position = navPoints[0];
		}
	}
	
	void Update () 
	{	
		Rotate();
		if(navPoints.Count > 1)
		{
			Move();	
		}
	}

	void OnDrawGizmos()
	{
		if(debugMode)
		{
			Gizmos.color = Color.blue;
			foreach (Vector3 point in navPoints)
			{
				Gizmos.DrawSphere(point, 0.5f);
			}
		}
	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.CompareTag("Player"))
		{
			AudioManager.Instance.Play("PlayerDeath");
			GameManager.Instance.KillPlayer();
		}
	}

	#endregion

	#region Class Methods

	private void Move()
	{
		transform.Translate(GetDirection() * Time.deltaTime * movementSpeed);
		if (HasReachedDestination())
		{
			if(nextPoint == navPoints.Count - 1)
			{
				nextPoint = 0;
			}
			else
			{
				nextPoint++;
			}
		}
	}

	private void Rotate()
	{
		rotatingBlade.transform.Rotate(bladeRO.Rotate());
	}

	private bool HasReachedDestination()
	{
		return Vector3.Magnitude(navPoints[nextPoint] - transform.position) <= 0.05f;
	}

	private Vector3 GetDirection()
	{
		return Vector3.Normalize(navPoints[nextPoint] - transform.position);
	}

	#endregion
}
