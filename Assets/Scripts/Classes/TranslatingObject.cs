﻿/*
* 
* Carlos Adan Cortes De la Fuente
* All rights reserved. Copyright (c) 
* Email: krlozadan@gmail.com
*
*/

using UnityEngine;

public class TranslatingObject
{
	#region Properties

	private float speed;
	private float xDistance;
	private float yDistance;

	private float xMoveSum;
	private float yMoveSum;
	private float xMoveDirection;
	private float yMoveDirection;

	private enum Direction 
	{
		Horizontal, 
		Vertical
	}

	#endregion

	#region Class Methods

	public TranslatingObject(float objectSpeed, float xDis, float yDis)
	{	
		speed = objectSpeed;
		xDistance = xDis;
		yDistance = yDis;
		xMoveSum = 0f;
		yMoveSum = 0f;
		xMoveDirection = xDistance >= 0 ? 1f : -1f;
		yMoveDirection = yDistance >= 0 ? 1f : -1f;
	}

	public Vector3 Move(Vector3 position)
	{
		Vector3 newPos = position;

		if(xDistance != 0)
		{
			float xMove = speed * Time.deltaTime * xMoveDirection;
			newPos.x += xMove;
			ChangeDirection(Direction.Horizontal, xMove);
		}

		if (yDistance != 0)
		{
			float yMove = speed * Time.deltaTime * yMoveDirection;
			newPos.y += yMove;
			ChangeDirection(Direction.Vertical, yMove);
		}
		
		return newPos;
	}

	private void ChangeDirection(Direction dir, float move)
	{	
		if(dir == Direction.Horizontal)
		{
			xMoveSum += Mathf.Abs(move);
			if (xMoveSum >= Mathf.Abs(xDistance))
			{
				xMoveDirection *= -1;
				xMoveSum = 0;
			}
		}
		else if (dir == Direction.Vertical)
		{
			yMoveSum += Mathf.Abs(move);
			if (yMoveSum >= Mathf.Abs(yDistance))
			{
				yMoveDirection *= -1;
				yMoveSum = 0;
			}
		}
	}

	#endregion
}