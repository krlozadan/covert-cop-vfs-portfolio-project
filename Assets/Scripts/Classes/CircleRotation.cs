﻿using UnityEngine;

public class CircleRotation : MonoBehaviour {

	#region Variables

	[SerializeField]
	private bool canRotate = true;
	[SerializeField]
	private float rotationSpeed = 50f;
	[SerializeField]
	private float maxAngle = 50f;

	private float angleSum = 0;
	private float direction = 1;

	#endregion 
	
	#region Unity Functions

	private void Awake()
	{
		// Rotates the object to the left to adjust the rotation max angle to the middle
		transform.Rotate(Vector3.forward * maxAngle / 2 * -1);	
	}
	
	void Update () 
	{
		if (canRotate)
		{
			RotateObject();	
		}
	}

	#endregion

	#region Class Methods

	void RotateObject()
	{
		angleSum += Mathf.Abs(rotationSpeed * Time.deltaTime * direction);
		if (angleSum >= maxAngle)
		{
			direction *= -1;
			angleSum = 0;
		} 
		Vector3 rotation = new Vector3(0, 0, rotationSpeed * Time.deltaTime * direction);
		transform.Rotate(rotation);
	}

	#endregion
}
