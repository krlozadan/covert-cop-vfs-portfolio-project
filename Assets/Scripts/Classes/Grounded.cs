﻿using UnityEngine;

public class Grounded 
{

	private float xOffset = 0f;
	private float yOffset = 0f;

	public Grounded()
	{
	}

	public void SetOffset(float offsetX, float offsetY)
	{
		xOffset = offsetX;
		yOffset = offsetY;
	}

	public bool IsGrounded(Vector3 origin, bool both)
	{
		if (both)
		{
			return CheckGrounded(origin, 1f) && CheckGrounded(origin, -1f);
		}
		else
		{
			return CheckGrounded(origin, 1f) || CheckGrounded(origin, -1f);
		}
	}

	public bool isSideGrounded(Vector3 origin, float direction)
	{
		return CheckGrounded(origin, direction);
	}

	private bool CheckGrounded(Vector3 origin, float direction)
	{
		origin.x += xOffset * direction;
		origin.y -= yOffset;
		float raycastDistance = 0.1f;
		Debug.DrawRay(origin, Vector3.down * raycastDistance, Color.red);
		return Physics2D.Raycast(origin, Vector2.down, raycastDistance, LayerMask.GetMask("Default"));
	}
}
