﻿/*
* 
* Carlos Adan Cortes De la Fuente
* All rights reserved. Copyright (c) 
* Email: krlozadan@gmail.com
*
*/

using UnityEngine;

public class RotatingObject 
{

	#region Properties

	private float speed;
	private bool clockwise;
	private float direction;
	
	#endregion

	#region Methods

	public RotatingObject(float rotationSpeed, bool turnsClockwise)
	{
		speed = rotationSpeed;
		clockwise = turnsClockwise;
		direction = -1f;
	}

	public Vector3 Rotate()
	{
		direction = clockwise ? -1f : 1f;
		return new Vector3(0, 0, speed * Time.deltaTime * direction);
	}	

	#endregion

}
