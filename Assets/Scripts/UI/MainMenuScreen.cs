﻿using UnityEngine;

public class MainMenuScreen : UIScreen 
{	
	protected override void UIScreenEnabled()
    {
		Cursor.lockState = CursorLockMode.Locked;
		if(AudioManager.Instance.IsPlaying("MenuMusic") == false)
		{
			AudioManager.Instance.Play("MenuMusic");
		}
    }

	public void OnStartGameMenuPressed()
	{
		UIManager.Instance.ShowScreen<LevelSelectScreen>();
	}

	public void OnControlsMenuPressed()
	{
		UIManager.Instance.ShowScreen<ControlsScreen>();
	}

	public void OnCreditsMenuPressed()
	{	
		UIManager.Instance.ShowScreen<CreditsScreen>();
	}

	public void OnExitMenuPressed()
	{
		Application.Quit();
	}

}