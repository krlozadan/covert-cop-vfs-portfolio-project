﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlsScreen : UIScreen 
{
    protected override void UIScreenEnabled() {}

	public void OnBackMenuPressed()
	{
		UIManager.Instance.ShowScreen<MainMenuScreen>();
	}

}
