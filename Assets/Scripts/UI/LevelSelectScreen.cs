﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelSelectScreen : UIScreen 
{

    protected override void UIScreenEnabled() {}

	public void OnTutorialMenuPressed()
	{
		SceneManager.LoadScene("Tutorial");
		UIManager.Instance.ShowScreen<GameScreen>();
		AudioManager.Instance.Stop("MenuMusic");
	}

	public void OnMainLevelMenuPressed()
	{
		SceneManager.LoadScene("MainLevel");
		UIManager.Instance.ShowScreen<GameScreen>();
		AudioManager.Instance.Stop("MenuMusic");
	}
	
	public void OnTestLevelMenuPressed()
	{
		SceneManager.LoadScene("EasyLevel");
		UIManager.Instance.ShowScreen<GameScreen>();
		AudioManager.Instance.Stop("MenuMusic");
	}

	public void OnBackMenuPressed()
	{
		UIManager.Instance.ShowScreen<MainMenuScreen>();
	}

}
