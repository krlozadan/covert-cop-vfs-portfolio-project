﻿using UnityEngine;

public class CreditsScreen : UIScreen 
{
    protected override void UIScreenEnabled() {}

    public void OnBackMenuPressed()
	{
		UIManager.Instance.ShowScreen<MainMenuScreen>();
	}
}
