﻿using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.UI;

public abstract class UIScreen : MonoBehaviour 
{
    [SerializeField]
	protected EventSystem eventSystem;
	[SerializeField]
	protected GameObject preselectedButton;

	private void OnEnable()
	{
		eventSystem.SetSelectedGameObject(null);
		if(preselectedButton != null)
		{
			eventSystem.SetSelectedGameObject(preselectedButton);
			Button button = preselectedButton.GetComponent<Button>();
			button.Select();
			button.OnSelect(null);
		}
		UIScreenEnabled();	
	}
	
	protected abstract void UIScreenEnabled();

}
