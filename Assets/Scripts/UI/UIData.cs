﻿using UnityEngine;

public class UIData : MonoBehaviour 
{
	private bool hacked = false;

	public void Hack()
	{
		hacked = true;
	}

	public void Reset()
	{
		hacked = false;
	}

}
