﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameScreen : UIScreen 
{

    private UIData[] uiData;

    protected override void UIScreenEnabled() 
    {
        if(AudioManager.Instance.IsPlaying("InGameMusic") == false)
		{
            AudioManager.Instance.Play("InGameMusic"); 
		}
        ResetData();
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape) || Input.GetButtonDown("Start"))
		{
            UIManager.Instance.ShowScreen<PauseScreen>();
		}
        UpdateHackedData();
    }

    private void UpdateHackedData()
    {
        for(int i = 0; i < Computer.computerList.Count; i++)
        {
            if (Computer.computerList[i].GetHackStatus() == 100.0f)
            {
                uiData[i].GetComponent<Image>().color = Color.green;
            }
            else
            {
                uiData[i].GetComponent<Image>().color = Color.red;
            }
        }
    }

    public void ResetData()
    {
        uiData = FindObjectsOfType<UIData>();
        foreach(UIData data in uiData)
        {
            data.GetComponent<Image>().color = new Color(0,0,0,0);
        }
    }

}
