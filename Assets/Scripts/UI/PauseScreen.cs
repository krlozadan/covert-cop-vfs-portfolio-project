﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseScreen : UIScreen 
{

	#region Unity Functions

		protected override void UIScreenEnabled() 
		{
			AudioManager.Instance.FadeSound("InGameMusic", 0.2f, 2f); // Fade Out
		}

	#endregion

	#region Pause Menu Functions

		private void PauseGame()
		{
			GameManager.Instance.PauseGame();
		}

		public void OnResumeGameButtonPressed()
		{
			GameManager.Instance.ResumeGame();
			AudioManager.Instance.FadeSound("InGameMusic", 1f, 2f); // Fade in	
			UIManager.Instance.ShowScreen<GameScreen>();
		}

		public void OnQuitGameButtonPressed()
		{
			if(GameManager.Instance != null)
			{
				GameManager.Instance.ResetComputers();
			}
			AudioManager.Instance.FadeSound("InGameMusic", 1f, 2f); // Fade in
			AudioManager.Instance.Stop("InGameMusic");
			UIManager.Instance.ShowScreen<MainMenuScreen>();
			SceneManager.LoadScene("MainMenu");
		}

    #endregion
}