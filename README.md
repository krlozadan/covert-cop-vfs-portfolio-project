# VFS Portfolio Project - Covert Cop (V0.1.0 - Prototype)
Covert Cop is a 2D side scrolling stealth based puzzle platformer for PC. Explore the layers of the Evil organization and find the evidence locked away securely that helps in exposing the organization to the people of the country. The player character start in one of the room of the building and can explore the entire building clearing it room by room and find evidence that is scattered around the building and finally get to the server room and download the files required to bring down the organization
  
## Team Members
+ Srikanth Venkatesan: Project Manager
+ Bharath Battu: Level Designer
+ Jack Chen: Artist
+ Carlos Adan: Programmer

## How to play
+ Arrows to move left & right
+ Space to jump
+ Double Space to double jump
+ Left Control to crouch
+ Left Shift to sprint
+ While sprinting, press Left Control to slide

### Notes
The assets being used are still work in progress
Code references:
+ [Unity Documentation](https://docs.unity3d.com/ScriptReference/index.html)
+ Previous VFS projects

Assets pulled from asset store:
+ [Double Sided Shader](https://assetstore.unity.com/packages/vfx/shaders/double-sided-shaders-23087)
+ [Text Mesh Pro](https://assetstore.unity.com/packages/essentials/beta-projects/textmesh-pro-84126)
